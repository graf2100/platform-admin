import ApolloClient, { gql } from 'apollo-boost'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { HttpLink } from 'apollo-link-http'

let URI = 'https://kitrum.dev/graphql'
let URI_DEVELOPMENT = 'http://localhost:5000/graphql'
const cache = new InMemoryCache()
export const client = new ApolloClient({
  uri: URI,
  link: new HttpLink(),
  cache,
})

export const REQUESTS = gql`
    query RequestsQuery
    {
        requests {
            id
            name
            text
            date
            isActive
        }
    }`


