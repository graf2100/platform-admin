/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Notifications from '@material-ui/icons/Notifications'
import Dashboard from '@material-ui/icons/Dashboard'
import Add from '@material-ui/icons/Add'
import NotesIcon from '@material-ui/icons/Notes'
import Storage from '@material-ui/icons/StorageRounded'
// core components/views for Admin layout
import AddRequest from './views/AddRequest/AddRequest'
import AddPost from './views/AddPost/'
import Requests from './views/Requests/Requests'
import DashboardPage from './views/Dashboard/Dashboard'
import Blog from './views/Blog/index'
import EditRequest from './views/EditRequest'
import EditPost from './views/EditPost'
import Notes from './views/Notes/Notes'
import TestAudience from './views/TestAudience'
// core components/views for RTL layout

const dashboardRoutes = [
  /*  {
      path: "/dashboard",
      name: "Dashboard",
      rtlName: "لوحة القيادة",
      icon: Dashboard,
      component: DashboardPage,
      layout: "/admin"
    },
    {
      path: "/user",
      name: "User Profile",
      rtlName: "ملف تعريفي للمستخدم",
      icon: Person,
      component: UserProfile,
      layout: "/admin"
    },
    {
      path: "/table",
      name: "Table List",
      rtlName: "قائمة الجدول",
      icon: "content_paste",
      component: TableList,
      layout: "/admin"
    },
    {
      path: "/typography",
      name: "Typography",
      rtlName: "طباعة",
      icon: LibraryBooks,
      component: Typography,
      layout: "/admin"
    },
    {
      path: "/icons",
      name: "Icons",
      rtlName: "الرموز",
      icon: BubbleChart,
      component: Icons,
      layout: "/admin"
    },
    {
      path: "/maps",
      name: "Maps",
      rtlName: "خرائط",
      icon: LocationOn,
      component: Maps,
      layout: "/admin"
    },
    {
      path: "/notifications",
      name: "Notifications",
      rtlName: "إخطارات",
      icon: Notifications,
      component: NotificationsPage,
      layout: "/admin"
    },
    {
      path: "/rtl-page",
      name: "RTL Support",
      rtlName: "پشتیبانی از راست به چپ",
      icon: Language,
      component: RTLPage,
      layout: "/rtl"
    },
    {
      path: "/upgrade-to-pro",
      name: "Upgrade To PRO",
      rtlName: "التطور للاحترافية",
      icon: Unarchive,
      component: UpgradeToPro,
      layout: "/admin"
    }*/
  {
    path: '/dashboard',
    name: 'Dashboard',
    rtlName: 'لوحة القيادة',
    icon: Dashboard,
    component: DashboardPage,
    layout: '/admin',
  },
  {
    path: '/requests',
    name: 'Requests',
    rtlName: '',
    icon: Notifications,
    component: Requests,
    layout: '/admin',
  }, {
    path: '/request/add',
    name: 'Add new request',
    rtlName: '',
    icon: Add,
    component: AddRequest,
    layout: '/admin',
  }, {
    path: '/blog',
    name: 'Posts',
    rtlName: '',
    icon: Storage,
    component: Blog,
    layout: '/admin',
  }, {
    path: '/post/add',
    name: 'Add new post',
    rtlName: '',
    icon: Add,
    component: AddPost,
    layout: '/admin',
  }, {
    path: '/edit/:id',
    name: 'Edit request',
    rtlName: '',
    icon: Add,
    component: EditRequest,
    layout: '/admin',
  }, {
    path: '/edit_post/:id',
    name: 'Edit post',
    rtlName: '',
    icon: Add,
    component: EditPost,
    layout: '/admin',
  }, {
    path: '/testaudience',
    name: 'TestAudience',
    rtlName: '',
    icon: NotesIcon,
    component: TestAudience,
    layout: '/admin',
  },
  /*{
    path: '/review',
    name: 'Review',
    rtlName: '',
    icon: RateReview,
    component: Requests,
    layout: '/admin'
  },
  {
    path: '/status',
    name: 'Status',
    rtlName: '',
    icon: Face,
    component: Requests,
    layout: '/admin'
  }*/
]

export default dashboardRoutes
