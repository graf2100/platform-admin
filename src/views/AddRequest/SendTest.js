import React, { Fragment, useState } from 'react'
import Button from '../../components/CustomButtons/Button'
import { gql } from 'apollo-boost'
import { Mutation } from 'react-apollo'
import AddAlert from "@material-ui/icons/AddAlert";
import Snackbar from '../../components/Snackbar/Snackbar'

const SEND_TEST_EMAIL = gql`
  mutation TestRequest( $name: String!) {
    testRequest(name:$name)
  }
`
const SendTest = ({ classes, name }) => {
  const [isOpen, setOpen] = useState(false)
  return (
    <Mutation mutation={SEND_TEST_EMAIL}
    >
      {(sendEmail, {}) => (<Fragment>
        <Button type="button" color="danger"
                onClick={() => {
                  sendEmail({
                    variables: { name }
                  })
                  setOpen(true)
                }} className={classes.buttonSendTest}>Send test email</Button>
        <Snackbar
          place="tr"
          color="info"
          icon={AddAlert}
          message="Письмо отправлено на почту!"
          open={isOpen}
          closeNotification={() => setOpen(false)}
          close
        />
      </Fragment>)}
    </Mutation>
  )
}

export default SendTest