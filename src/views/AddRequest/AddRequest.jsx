/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useState, useEffect } from 'react'
// nodejs library to set properties for components
import PropTypes from 'prop-types'
// @material-ui/core components
// core components
import GridItem from '../../components/Grid/GridItem.jsx'
import GridContainer from '../../components/Grid/GridContainer.jsx'
import Card from '../../components/Card/Card.jsx'
import CardBody from '../../components/Card/CardBody.jsx'
import Chip from '@material-ui/core/Chip'
import { gql } from 'apollo-boost'
import { Mutation } from 'react-apollo'
import Button from '../../components/CustomButtons/Button.jsx'
import CustomInput from '../../components/CustomInput/CustomInput'
import makeStyles from '@material-ui/core/styles/makeStyles'
import 'froala-editor/js/froala_editor.pkgd.min.js'
import 'froala-editor/js/plugins.pkgd.min.js'
import 'froala-editor/js/plugins/align.min.js'
// Require Editor CSS files.
import 'froala-editor/css/froala_style.min.css'
import 'froala-editor/css/froala_editor.pkgd.min.css'
import { REQUESTS } from '../../config'
import FroalaEditor from 'react-froala-wysiwyg'
import SendTest from './SendTest'
import uuidv1 from 'uuid/v1'
import Select from '@material-ui/core/Select'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Avatar from '@material-ui/core/Avatar'
import MenuItem from '@material-ui/core/MenuItem'
import { useMutation } from '@apollo/react-hooks'

const useStyles = makeStyles(theme => ({
  textField: {
    margin: theme.spacing(0),
  },
  selectField: {
    marginBottom: theme.spacing(2),
    width:'100%'
  },
  buttonSend: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  }, buttonSendTest: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    marginLeft: theme.spacing(4),
  },
}))

const ADD_REQUEST = gql`
    mutation AddRequest($name: String! $text: String! $author: String! $link: String $tags: [String]!) {
        addRequest(name: $name text: $text link: $link tags:$tags author:$author) {
            id
            name
            isActive
        }
    }
`
const ADD_NOTES = gql`
    mutation AddNote($name:String!, $text:String!, $indication:String!) {
        addNote(name:$name,text:$text, indication:$indication) {
            name
            indication
            text
        }
    }
`
let NOTES = gql`
    query RequestsQuery {
        notes {
            name
            indication
            text
        }
    }

`

function Requests ({ history, location }) {
  const classes = useStyles()
  const [text, setText] = useState('')
  const [isLoading, setLoading] = useState(true)
  const [name, setName] = useState('')
  const [author, setAuthor] = useState('')
  const [tags, setTags] = useState([])
  const [tag, setTag] = useState('')
  const [indication, setIndication] = useState(uuidv1())
  const [addNote,] = useMutation(ADD_NOTES,
    {
      update (cache, { data: { addNote } }) {
        try {
          const { notes } = cache.readQuery({ query: NOTES })
          let index = notes.findIndex(note => note.indication === addNote.indication)
          if (index > -1) {
            notes[index] = ({ ...addNote })
          } else {
            notes.push(addNote)
          }
          cache.writeQuery({
            query: NOTES,
            data: { notes: [...notes] },
          })
        } catch (e) {
          console.error(e)
        }
      }
    })
  useEffect(() => {
    if (location.state) {
      if (location.state.name) {
        setName(location.state.name)
      }
      if (location.state.indication) {
        setIndication(location.state.indication)
      }
      if (location.state.text) {
        setText(location.state.text)
      }
      setLoading(false)
    } else {
      setLoading(false)
    }

  }, [location])

  useEffect(() => {
    if (text || name) {
      if (location.state) {
        if (location.state.name === name && location.state.text === text) {
          return
        }
      }
      addNote({
        variables: { text, name, indication },
      })
    }
  }, [text, name, location])

  if (isLoading) {
    return null
  }
  return (
    <Mutation mutation={ADD_REQUEST}
              update={(cache, { data: { addRequest } }) => {
                try {
                  const next = cache.readQuery({ query: REQUESTS })
                  const requests = next ? next.requests
                    ? next.requests
                    : undefined : undefined
                  if (requests) {
                    cache.writeQuery({
                      query: REQUESTS,
                      data: { requests: [...requests, addRequest] },
                    })
                  }
                } catch (e) {
                  console.error(e)
                } finally {
                  history.push('/admin/requests')
                }
              }}
    >
      {(addRequest, { data }) => (
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardBody>
                <CustomInput
                  labelText="Name of new request"
                  id="float"
                  formControlProps={{
                    fullWidth: true,
                    className: classes.textField,
                  }}
                  inputProps={{
                    value: name,
                    onChange: event => {
                      console.log(event.target.value)
                      setName(event.target.value)
                    },
                  }}
                />
                <div style={{ margin: '1rem 0 ' }}>
                  {tags.map((tag, index) => <Chip key={'tag' + index} label={tag}
                                                  style={{ margin: '0.3rem' }}
                                                  avatar={<Avatar>{tag[0]}</Avatar>}
                                                  onDelete={() => {
                                                    setTags(tags.filter((_tag, _index) => _index !== index))
                                                  }}/>)}

                </div>
                <CustomInput
                  labelText="Add tag"
                  id="float"
                  formControlProps={{
                    fullWidth: true,
                    className: classes.textField,
                  }}
                  inputProps={{
                    value: tag,
                    onChange: event => {
                      setTag(event.target.value)
                    },
                    onKeyDown: event => {
                      if (event.key === 'Enter') {
                        setTags([...tags, tag])
                        setTag('')
                      }
                    }
                  }}
                />
                <FormControl className={classes.selectField}>
                  <InputLabel id="demo-simple-select-label">Author of new request</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={author}
                    onChange={event => setAuthor(event.target.value)}
                  >
                    <MenuItem value={''}>-</MenuItem>
                    <MenuItem value={'Валентина'}>Валентина</MenuItem>
                    <MenuItem value={'Алёна'}>Алёна</MenuItem>
                  </Select>
                </FormControl>
                <FroalaEditor
                  model={text}
                  onModelChange={setText}
                />
                <Button type="button" color="rose" onClick={() => {
                  addRequest({ variables: { name, text, link: '', author, tags } })
                }} className={classes.buttonSend}>Add request</Button>
                <SendTest classes={classes} name={name}/>

              </CardBody>
            </Card>

          </GridItem>

        </GridContainer>
      )}</Mutation>
  )
}

Requests.propTypes = {
  classes: PropTypes.object,
}

export default Requests
