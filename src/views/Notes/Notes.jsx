/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react'
// nodejs library to set properties for components
import PropTypes from 'prop-types'
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles'
// core components
import GridItem from '../../components/Grid/GridItem.jsx'

import GridContainer from 'components/Grid/GridContainer.jsx'
import Table from 'components/Table/Table.jsx'
import Card from 'components/Card/Card.jsx'
import CardHeader from 'components/Card/CardHeader.jsx'
import CardBody from 'components/Card/CardBody.jsx'
import Delete from '@material-ui/icons/Delete'
import Edit from '@material-ui/icons/Edit'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import RefreshIcon from '@material-ui/icons/Refresh'
import IconButton from '@material-ui/core/IconButton'
import { REQUESTS } from '../../config'
import { gql } from 'apollo-boost'
import { Mutation, Query } from 'react-apollo'
import { Link } from 'react-router-dom'
import moment from 'moment'
import MaterialTable from 'material-table'
import tableIcons from '../../config/tableIcons'

let NOTES = gql`
  query RequestsQuery {
    notes {
      name
      indication
      text
    }
  }

`
let REMOVE_NOTE = gql`
  mutation RemoveNote($indication:String!) {
    removeNote(indication:$indication)
  }
`

const styles = {
  cardCategoryWhite: {
    '&,& a,& a:hover,& a:focus': {
      color: 'rgba(255,255,255,.62)',
      margin: '0',
      fontSize: '14px',
      marginTop: '0',
      marginBottom: '0',
    },
    '& a,& a:hover,& a:focus': {
      color: '#FFFFFF',
    },
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: '\'Roboto\', \'Helvetica\', \'Arial\', sans-serif',
    marginBottom: '3px',
    textDecoration: 'none',
    '& small': {
      color: '#777',
      fontSize: '65%',
      fontWeight: '400',
      lineHeight: '1',
    },
  },
}

function Notes (props) {
  const { classes } = props

  const DeleteBtn = ({ indication }) => (
    <Mutation key={'Delete' + indication} mutation={REMOVE_NOTE} update={(
      cache,
      { data: {} }) => {
      try {
        let { notes } = cache.readQuery({ query: NOTES })
        notes = notes.filter(note => note.indication !== indication)
        cache.writeQuery({
          query: NOTES,
          data: { notes: [...notes] },
        })
      } catch (e) {
        console.error(e)
      } finally {
        props.history.push('/admin/notes')
      }
    }}>
      {(removeNote, { data }) => (
        <IconButton onClick={() => removeNote({ variables: { indication } })}
                    size="small"
                    aria-label="edit"
        ><Delete fontSize="inherit"/>
        </IconButton>)
      }</Mutation>)

  return (<Query query={NOTES}>
    {({ loading, error, data }) => {
      if (loading) return 'Loading...'
      if (error) return `Error! ${error.message}`
      return (
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Notes</h4>
                <p className={classes.cardCategoryWhite}>
                  Here is notes for this table
                </p>
              </CardHeader>
              <CardBody>
                <MaterialTable
                  title={''}
                  icons={tableIcons}
                  options={{ actionsColumnIndex: true, pageSize: 5, pageSizeOptions: [5, 10, 15] }}
                  columns={[
                    {
                      title: 'Title', field: 'name', render: note => <Link to={{
                        pathname: '/admin/request/add',
                        state: { ...note }
                      }}>{note.name}</Link>
                    },
                    { title: 'Delete', field: 'indication', render: note => <DeleteBtn indication={note.indication}/> },
                  ]
                  } data={data.notes}
                />
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>)
    }}</Query>)
}

Notes.propTypes = {
  classes: PropTypes.object,
}

export default withStyles(styles)(Notes)
