/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react'
// nodejs library to set properties for components
import PropTypes from 'prop-types'
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles'
// core components
import GridItem from '../../components/Grid/GridItem.jsx'

import GridContainer from 'components/Grid/GridContainer.jsx'
import Table from 'components/Table/Table.jsx'
import Card from 'components/Card/Card.jsx'
import CardHeader from 'components/Card/CardHeader.jsx'
import CardBody from 'components/Card/CardBody.jsx'
import Delete from '@material-ui/icons/Delete'
import Edit from '@material-ui/icons/Edit'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import RefreshIcon from '@material-ui/icons/Refresh'
import IconButton from '@material-ui/core/IconButton'
import { REQUESTS } from '../../config'
import { gql } from 'apollo-boost'
import { Mutation, Query } from 'react-apollo'
import { Link } from 'react-router-dom'
import moment from 'moment'
import MaterialTable from 'material-table'
import tableIcons from '../../config/tableIcons'

let CHANGE_ACTIVE = gql`
    mutation ChangeActive($id:  String!){
        changeActive(id: $id){
            id
        }
    }
`
let REMOVE_REQUEST = gql`
    mutation RemoveRequest($id:  String!){
        removeRequest(id: $id){
            id
        }
    }
`
let REOPEN_REQUEST = gql`
    mutation ReopenRequest($id:  String!){
        reopenRequest(id: $id){
            id
        }
    }
`

const styles = {
  cardCategoryWhite: {
    '&,& a,& a:hover,& a:focus': {
      color: 'rgba(255,255,255,.62)',
      margin: '0',
      fontSize: '14px',
      marginTop: '0',
      marginBottom: '0',
    },
    '& a,& a:hover,& a:focus': {
      color: '#FFFFFF',
    },
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: '\'Roboto\', \'Helvetica\', \'Arial\', sans-serif',
    marginBottom: '3px',
    textDecoration: 'none',
    '& small': {
      color: '#777',
      fontSize: '65%',
      fontWeight: '400',
      lineHeight: '1',
    },
  },
}

function Requests (props) {
  const { classes } = props

  const EditBtn = ({ id }) => <Link to={'/admin/edit/' + id}><IconButton
    size="small"
    aria-label="edit"><Edit
    fontSize="inherit"/></IconButton></Link>

  const DeleteBtn = ({ id }) => (
    <Mutation key={'Delete' + id} mutation={REMOVE_REQUEST} update={(
      cache,
      { data: { removeRequest } }) => {
      const { requests } = cache.readQuery({ query: REQUESTS })
      let index = requests.findIndex(request => request.id === removeRequest.id)
      if (index !== -1) {
        requests.splice(index, 1)
        cache.writeQuery({
          query: REQUESTS,
          data: { requests: [...requests] },
        })
        props.history.push('/admin/requests')
      }
    }}>
      {(removeRequest, { data }) => (
        <IconButton onClick={() => removeRequest({ variables: { id } })}
                    size="small"
                    aria-label="edit"
        ><Delete fontSize="inherit"/>
        </IconButton>)
      }</Mutation>)

  const ReopenBtn = ({ id }) => (
    <Mutation key={'Reopen' + id}
              mutation={REOPEN_REQUEST}
              update={(cache, data) => {
                try {
                  const { requests } = cache.readQuery({ query: REQUESTS })
                  cache.writeQuery({
                    query: REQUESTS,
                    data: {
                      requests: requests.map(request => {
                        console.log(request)
                        if (request.id === id) {
                          console.log(request.name)
                          request.isActive = true
                        }
                        return request
                      })
                    },
                  })
                } catch (e) {
                  console.log()
                } finally {
                  alert('Запрос отправлен заново!')
                }
              }}
    >
      {(reopenRequest, { data }) => (
        <IconButton
          onClick={() => reopenRequest({ variables: { id } })}
          size="small" aria-label="edit">
          <RefreshIcon
            fontSize="inherit"/>
        </IconButton>)}
    </Mutation>)

  const VisibilityBtn = ({ index, isActive, id }) => (
    <Mutation mutation={CHANGE_ACTIVE} key={'Visibility' + index + id} update={(
      cache,
      { data: { changeActive } }) => {
      const { requests } = cache.readQuery({ query: REQUESTS })
      let index = requests.findIndex(request => request.id === changeActive.id)
      if (index !== -1) {
        requests[index].isActive = !requests[index].isActive
        cache.writeQuery({
          query: REQUESTS,
          data: { requests: [...requests] },
        })
        props.history.push('/admin/requests')
      }
    }}>
      {(changeActive, { data }) => (
        <IconButton size="small"
                    onClick={() => changeActive({ variables: { id } })}
                    aria-label="edit">
          {isActive ? <Visibility fontSize="inherit"/> : <VisibilityOff
            fontSize="inherit"/>}
        </IconButton>
      )}</Mutation>)

  return (<Query query={REQUESTS}>
    {({ loading, error, data }) => {
      if (loading) return 'Loading...'
      if (error) return `Error! ${error.message}`
      return (
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Requests</h4>
                <p className={classes.cardCategoryWhite}>
                  Here is requests for this table
                </p>
              </CardHeader>
              <CardBody>
                <MaterialTable
                  title={''}
                  icons={tableIcons}
                  onSearchChange={text => {
                    console.log('text')
                    console.log(text)
                  }}
                  options={{ pageSize: 50, search: true, pageSizeOptions: [50, 100, 150] }}
                  columns={[
                    {
                      title: 'Title', field: 'name',
                    },
                    {
                      title: 'Date',
                      field: 'date',
                      render: request => moment(parseInt(request.date)).format('L')
                    },
                    {
                      title: 'Visibility', field: 'isActive', searchable: false,
                      render: request => <VisibilityBtn id={request.id}
                                                        isActive={request.isActive}/>
                    },
                    {
                      title: 'Reopen',
                      field: 'id',
                      render: request => <ReopenBtn id={request.id}/>
                    },
                    { title: 'Edit', field: 'id', searchable: false, render: request => <EditBtn id={request.id}/> },
                    {
                      title: 'Delete',
                      field: 'id',
                      render: request => <DeleteBtn id={request.id}/>
                    },
                    {
                      title: 'Text',
                      field: 'text',
                      searchable: true,
                      hidden: true,
                    },
                  ]
                  } data={data.requests}
                />
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>)
    }}</Query>)
}

Requests.propTypes = {
  classes: PropTypes.object,
}

export default withStyles(styles)(Requests)
