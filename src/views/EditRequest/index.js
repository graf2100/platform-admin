/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react'
// nodejs library to set properties for components
// @material-ui/core components
// core components
import { gql } from 'apollo-boost'
import { Query } from 'react-apollo'
import makeStyles from '@material-ui/core/styles/makeStyles'
import 'froala-editor/js/froala_editor.pkgd.min.js'
import 'froala-editor/js/plugins.pkgd.min.js'
import 'froala-editor/js/plugins/align.min.js'
// Require Editor CSS files.
import 'froala-editor/css/froala_style.min.css'
import 'froala-editor/css/froala_editor.pkgd.min.css'
import Edit from './edit'

const useStyles = makeStyles(theme => ({
  textField: {
    margin: theme.spacing(0),
  },
  buttonSend: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}))

const EDIT_REQUEST = gql`
    query RequestQuery($id: String!){
        request (id : $id ){
            id
            name
            text
            link
            author
            tags
        }
    }`

function Requests ({ match: { params: { id } }, history }) {
  return (<Query query={EDIT_REQUEST} variables={{ id }}>
    {({ loading, error, data }) => {
      if (loading) return null
      if (error) return `Error! ${error}`
      return (<Edit request={data.request} history={history}/>)
    }}
  </Query>)
}

/*return (
  <Mutation mutation={ADD_REQUEST}
            update={(cache, { data: { addRequest } }) => {
              const next = cache.readQuery({ query: REQUESTS })
              const requests = next ? next.requests
                ? next.requests
                : undefined : undefined
              if (requests) {
                cache.writeQuery({
                  query: REQUESTS,
                  data: { requests: [...requests, addRequest] },
                })
              }
              this.props.history.push('/admin/requests')
            }}
  >
    {(addRequest, { data }) => (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardBody>
              <CustomInput
                labelText="Name of new request"
                id="float"
                formControlProps={{
                  fullWidth: true,
                  className: classes.textField,
                }}
                inputProps={{
                  onChange: event => {
                    console.log(event.target.value)
                    setName(event.target.value)
                  },
                }}
              />
              <FroalaEditor tag='textarea'

                            model={text}
                            onModelChange={setText}
              />
              <Button type="button" color="rose" onClick={() => {
                addRequest({ variables: { name, text, link: '' } })
              }} className={classes.buttonSend}>Add request</Button>
            </CardBody>
          </Card>

        </GridItem>

      </GridContainer>
    )}</Mutation>*/

export default Requests
