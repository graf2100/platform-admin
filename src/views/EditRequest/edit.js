/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useEffect, useState } from 'react'
// nodejs library to set properties for components
import PropTypes from 'prop-types'
// @material-ui/core components
// core components
import GridItem from '../../components/Grid/GridItem.jsx'
import GridContainer from '../../components/Grid/GridContainer.jsx'
import Card from '../../components/Card/Card.jsx'
import CardBody from '../../components/Card/CardBody.jsx'
import { gql } from 'apollo-boost'
import { Mutation } from 'react-apollo'
import Button from '../../components/CustomButtons/Button.jsx'
import CustomInput from '../../components/CustomInput/CustomInput'
import makeStyles from '@material-ui/core/styles/makeStyles'
import 'froala-editor/js/froala_editor.pkgd.min.js'
import 'froala-editor/js/plugins.pkgd.min.js'
import 'froala-editor/js/plugins/align.min.js'
// Require Editor CSS files.
import 'froala-editor/css/froala_style.min.css'
import 'froala-editor/css/froala_editor.pkgd.min.css'
import FroalaEditor from 'react-froala-wysiwyg'
import Chip from '@material-ui/core/Chip'
import Avatar from '@material-ui/core/Avatar'
import InputLabel from '@material-ui/core/InputLabel'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'

const useStyles = makeStyles(theme => ({
  textField: {
    margin: theme.spacing(0),
  },
  selectField: {
    marginBottom: theme.spacing(2),
    width: '100%'
  },
  buttonSend: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}))

const EDIT_REQUEST = gql`
    mutation EditRequest($id:String! $name: String! $text: String! $link: String $tags: [String]! $author: String!) {
        editRequest(id: $id name: $name text: $text link: $link tags:$tags author:$author) {
            id
            name
            link
            text
            tags
            author
            isActive
        }
    }
`
const REQUESTS = gql`
    query RequestsQuery
    {
        requests {
            id
            name
            isActive
        }
    }`

function Requests ({ request, history }) {
  const classes = useStyles()
  const [text, setText] = useState(request.text)
  const [name, setName] = useState('')
  const [tags, setTags] = useState(request.tags || [])
  const [tag, setTag] = useState('')
  const [author, setAuthor] = useState(request.author)
  useEffect(() => {
    if (request) {
      setName(request.name)
      setText(request.text)
      setTags(request.tags || [])
      setAuthor(request.author || '')
    }
  }, [request])
  return (
    <Mutation mutation={EDIT_REQUEST}
              update={(cache, { data: { editRequest } }) => {
                try {
                  const { requests } = cache.readQuery({ query: REQUESTS })
                  let index = requests.findIndex(request => request.id === editRequest.id)
                  if (index !== -1) {
                    requests.splice(index, 1, editRequest)
                    cache.writeQuery({
                      query: REQUESTS,
                      data: { requests: [...requests] },
                    })
                  }
                } catch (e) {
                  console.log(e)
                } finally {
                  history.push('/admin/requests/')
                }
              }}
    >
      {(editRequest, { data }) => (
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardBody>
                <CustomInput
                  labelText="Name of request"
                  id="float"
                  formControlProps={{
                    fullWidth: true,
                    className: classes.textField,
                  }}
                  inputProps={{
                    value: name,
                    onChange: event => {
                      console.log(event.target.value)
                      setName(event.target.value)
                    },
                  }}
                />
                <div style={{ margin: '1rem 0 ' }}>
                  {tags.map((tag, index) => <Chip key={'tag' + index} label={tag}
                                                  style={{ margin: '0.3rem' }}
                                                  avatar={<Avatar>{tag[0]}</Avatar>}
                                                  onDelete={() => {
                                                    setTags(tags.filter((_tag, _index) => _index !== index))
                                                  }}/>)}

                </div>
                <CustomInput
                  labelText="Add tag"
                  id="float"
                  formControlProps={{
                    fullWidth: true,
                    className: classes.textField,
                  }}
                  inputProps={{
                    value: tag,
                    onChange: event => {
                      setTag(event.target.value)
                    },
                    onKeyDown: event => {
                      if (event.key === 'Enter') {
                        setTags([...tags, tag])
                        setTag('')
                      }
                    }
                  }}
                />
                <FormControl className={classes.selectField}>
                  <InputLabel id="demo-simple-select-label">Author of new request</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={author}
                    onChange={event => setAuthor(event.target.value)}
                  >
                    <MenuItem value={''}>-</MenuItem>
                    <MenuItem value={'Валентина'}>Валентина</MenuItem>
                    <MenuItem value={'Алёна'}>Алёна</MenuItem>
                  </Select>
                </FormControl>
                <FroalaEditor
                  model={text}
                  onModelChange={setText}
                />
                <Button type="button" color="rose" onClick={() => {
                  console.log('author', author)
                  editRequest({ variables: { id: request.id, name, text, link: '', tags, author } })
                }} className={classes.buttonSend}>Edit request</Button>
              </CardBody>
            </Card>

          </GridItem>

        </GridContainer>
      )}</Mutation>
  )
}

Requests.propTypes = {
  classes: PropTypes.object,
}

export default Requests
