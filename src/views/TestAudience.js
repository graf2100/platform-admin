import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Button from '../components/CustomButtons/Button'
import Modal from '@material-ui/core/Modal'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import FormLabel from '@material-ui/core/FormLabel'
import FormControl from '@material-ui/core/FormControl'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import Checkbox from '@material-ui/core/Checkbox'

function getSteps () {
  return ['Select campaign settings', 'Create an ad group', 'Create an ad']
}

function getStepContent (step) {
  switch (step) {
    case 0:
      return 'Select campaign settings...'
    case 1:
      return 'What is an ad group anyways?'
    case 2:
      return 'This is the bit I really care about!'
    default:
      return 'Unknown step'
  }
}

const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    width: 900,
    top: '8rem',
    left: 'calc(50% - 450px)',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}))
const TestAudience = () => {
  const [open, setOpen] = useState(false)
  const [active, setActive] = useState(0)
  const [useAll, setAll] = useState(false)
  const [useCompanies, setCompanies] = useState(false)
  const [useUsers, setUsers] = useState(false)
  const [useTags, setTags] = useState(false)
  const classes = useStyles()
  return (
    <div>
      <Button type="button" color="rose" onClick={() => {
        setOpen(true)
      }}>Select audience</Button>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={() => setOpen(false)}
      >
        <div className={classes.paper}>
          <Stepper activeStep={active}>
            <Step completed={active > 0}>
              <StepLabel>Types</StepLabel>
            </Step>
            {useCompanies && (
              <Step completed={active > 1}>
                <StepLabel>Select Company</StepLabel>
              </Step>)}
            {useTags && (
              <Step completed={active > 2}>
                <StepLabel>Select Tags</StepLabel>
              </Step>)}
            {useUsers && (
              <Step completed={active > 3}>
                <StepLabel>Select Users</StepLabel>
              </Step>
            )}
            <Step completed={false}>
              <StepLabel>Audience</StepLabel>
            </Step>
          </Stepper>
          {active === 0 && (
            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel component="legend">Select types of audience</FormLabel>
              <FormGroup>
                <FormControlLabel
                  control={<Checkbox checked={useAll} onChange={() => {
                    setAll(!useAll)
                    setCompanies(false)
                    setTags(false)
                    setUsers(false)
                  }
                  } value="useAll"/>}
                  label="All"
                />
                <FormControlLabel
                  control={
                    <Checkbox checked={useCompanies} onChange={() => {
                      setAll(false)
                      setCompanies(!useCompanies)
                    }} value="useCompanies"/>
                  }
                  label="Companies"
                />
                <FormControlLabel
                  control={
                    <Checkbox checked={useTags} onChange={() => {
                      setAll(false)
                      setTags(!useTags)
                    }} value="useTags"/>
                  }
                  label="Tags"
                />
                <FormControlLabel
                  control={
                    <Checkbox checked={useUsers} onChange={() => {
                      setAll(false)
                      setUsers(!useUsers)
                    }} value="useUsers"/>
                  }
                  label="Users"
                />
              </FormGroup>
            </FormControl>
          )}
        </div>
      </Modal>
    </div>
  )
}

export default TestAudience
