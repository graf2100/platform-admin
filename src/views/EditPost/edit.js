import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
// @material-ui/core components
// core components
import GridItem from '../../components/Grid/GridItem.jsx'
import GridContainer from '../../components/Grid/GridContainer.jsx'
import Card from '../../components/Card/Card.jsx'
import CardBody from '../../components/Card/CardBody.jsx'
import { gql } from 'apollo-boost'
import { Mutation } from 'react-apollo'
import Button from '../../components/CustomButtons/Button.jsx'
import CustomInput from '../../components/CustomInput/CustomInput'
import makeStyles from '@material-ui/core/styles/makeStyles'
import 'froala-editor/js/froala_editor.pkgd.min.js'
import 'froala-editor/js/plugins.pkgd.min.js'
import 'froala-editor/js/plugins/align.min.js'
// Require Editor CSS files.
import 'froala-editor/css/froala_style.min.css'
import 'froala-editor/css/froala_editor.pkgd.min.css'
import FroalaEditor from 'react-froala-wysiwyg'

const useStyles = makeStyles(theme => ({
  textField: {
    margin: theme.spacing(0),
  },
  buttonSend: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}))

const EDIT_POST = gql`
  mutation EditPost($id:String! $name: String! $text: String!) {
    editPost(id: $id name: $name text: $text) {
      id
      name
      date
    }
  }
`
const BLOG = gql`
query Blog{
  blog{
    id
    name
    date
  }
}
`

function Requests ({ post, history }) {
  const classes = useStyles()
  const [text, setText] = useState(post.text)
  const [name, setName] = useState('')
  useEffect(() => {
    if (post) {
      setName(post.name)
      setText(post.text)
    }
  }, [post])

  return (
    <Mutation mutation={EDIT_POST}
              update={(cache, { data: { editPost } }) => {
                try {
                  const { blog } = cache.readQuery({ query: BLOG })
                  let index = blog.findIndex(
                    request => request.id === editPost.id)
                  if (index !== -1) {
                    blog.splice(index, 1, editPost)
                    cache.writeQuery({
                      query: BLOG,
                      data: { blog: [...blog] },
                    })

                  }
                } catch (e) {
                  console.error(e)
                } finally {
                  history.push('/admin/blog/')
                }
              }}
    >
      {(editPost, { data }) => (
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardBody>
                <CustomInput
                  labelText="Name of post"
                  id="float"
                  formControlProps={{
                    fullWidth: true,
                    className: classes.textField,
                  }}
                  inputProps={{
                    value: name,
                    onChange: event => {
                      console.log(event.target.value)
                      setName(event.target.value)
                    },
                  }}
                />
                <FroalaEditor
                  model={text}
                  onModelChange={setText}
                />
                <Button type="button" color="rose" onClick={() => {
                  editPost(
                    { variables: { id: post.id, name, text } })
                }} className={classes.buttonSend}>Edit post</Button>
              </CardBody>
            </Card>

          </GridItem>

        </GridContainer>
      )}</Mutation>
  )
}

Requests.propTypes = {
  classes: PropTypes.object,
}

export default Requests
