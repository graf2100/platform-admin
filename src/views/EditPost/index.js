import React from 'react'
// nodejs library to set properties for components
// @material-ui/core components
// core components
import { gql } from 'apollo-boost'
import { Query } from 'react-apollo'
import makeStyles from '@material-ui/core/styles/makeStyles'
import 'froala-editor/js/froala_editor.pkgd.min.js'
import 'froala-editor/js/plugins.pkgd.min.js'
import 'froala-editor/js/plugins/align.min.js'
// Require Editor CSS files.
import 'froala-editor/css/froala_style.min.css'
import 'froala-editor/css/froala_editor.pkgd.min.css'
import Edit from './edit'

const useStyles = makeStyles(theme => ({
  textField: {
    margin: theme.spacing(0),
  },
  buttonSend: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}))

const EDIT_POST = gql`
  query Post($id: String!){
    post(id : $id ){
      id
      name
      text
      date
    }
  }`

function EditPost ({ match: { params: { id } }, history }) {
  return (<Query query={EDIT_POST} variables={{ id }}>
    {({ loading, error, data }) => {
      if (loading) return null
      if (error) return `Error! ${error}`
      return (<Edit post={data.post} history={history}/>)
    }}
  </Query>)
}

export default EditPost
