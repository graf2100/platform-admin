/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useState } from 'react'
// nodejs library to set properties for components
// @material-ui/core components
// core components
import GridItem from '../../components/Grid/GridItem.jsx'
import GridContainer from '../../components/Grid/GridContainer.jsx'
import Card from '../../components/Card/Card.jsx'
import CardBody from '../../components/Card/CardBody.jsx'
import { gql } from 'apollo-boost'
import { Mutation } from 'react-apollo'
import Button from '../../components/CustomButtons/Button.jsx'
import CustomInput from '../../components/CustomInput/CustomInput'
import makeStyles from '@material-ui/core/styles/makeStyles'
import 'froala-editor/js/froala_editor.pkgd.min.js'
import 'froala-editor/js/plugins.pkgd.min.js'
import 'froala-editor/js/plugins/align.min.js'
// Require Editor CSS files.
import 'froala-editor/css/froala_style.min.css'
import 'froala-editor/css/froala_editor.pkgd.min.css'
import FroalaEditor from 'react-froala-wysiwyg'
import { REQUESTS } from '../../config'

const useStyles = makeStyles(theme => ({
  textField: {
    margin: theme.spacing(0),
  },
  buttonSend: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}))

const ADD_POST = gql`
  mutation AddBlog($name: String! $text: String! ) {
    addBlog(name: $name text: $text) {
      id
      name
      date
    }
  }
`
const BLOG = gql`
  query Blog{
    blog{
      id
      name
      date
    }
  }
`
function AddPost (props) {
  const classes = useStyles()
  const [text, setText] = useState('')
  const [name, setName] = useState('')
  return (
    <Mutation mutation={ADD_POST}
              update={(cache, { data }) => {
                const next = cache.readQuery({ query: BLOG })
                if (next.blog) {
                  cache.writeQuery({
                    query: BLOG,
                    data: { blog: [...next.blog, data.addBlog] },
                  })
                }
                props.history.push('/admin/blog/')
              }}
    >
      {(addBlog, { data }) => (
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardBody>
                <CustomInput
                  labelText="Name of new request"
                  id="float"
                  formControlProps={{
                    fullWidth: true,
                    className: classes.textField,
                  }}
                  inputProps={{
                    onChange: event => {
                      console.log(event.target.value)
                      setName(event.target.value)
                    },
                  }}
                />
                <FroalaEditor tag='textarea'
                              model={text}
                              onModelChange={setText}
                />
                <Button type="button" color="rose" onClick={() => {
                  addBlog({ variables: { name, text } })
                }} className={classes.buttonSend}>Add</Button>
              </CardBody>
            </Card>

          </GridItem>

        </GridContainer>
      )}</Mutation>
  )
}

export default AddPost
