/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react'
// nodejs library to set properties for components
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles'
// core components
import GridItem from '../../components/Grid/GridItem.jsx'
import moment from 'moment'
import GridContainer from 'components/Grid/GridContainer.jsx'
import Table from 'components/Table/Table.jsx'
import Card from 'components/Card/Card.jsx'
import CardHeader from 'components/Card/CardHeader.jsx'
import CardBody from 'components/Card/CardBody.jsx'
import Delete from '@material-ui/icons/Delete'
import IconButton from '@material-ui/core/IconButton'
import { gql } from 'apollo-boost'
import { Mutation, Query } from 'react-apollo'
import { Link } from 'react-router-dom'
import Edit from '@material-ui/icons/Edit'

const REMOVE_POST = gql`
  mutation removeBlog($id:  String!){
  removeBlog(id: $id){
    id
  }
}
`
const BLOG = gql`
query Blog{
  blog{
    id
    name
    date
  }
}
`
const styles = {
  cardCategoryWhite: {
    '&,& a,& a:hover,& a:focus': {
      color: 'rgba(255,255,255,.62)',
      margin: '0',
      fontSize: '14px',
      marginTop: '0',
      marginBottom: '0',
    },
    '& a,& a:hover,& a:focus': {
      color: '#FFFFFF',
    },
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: '\'Roboto\', \'Helvetica\', \'Arial\', sans-serif',
    marginBottom: '3px',
    textDecoration: 'none',
    '& small': {
      color: '#777',
      fontSize: '65%',
      fontWeight: '400',
      lineHeight: '1',
    },
  },
}

function Blog (props) {
  const { classes } = props

  const EditBtn = ({ id }) => <Link to={'/admin/edit_post/' + id}><IconButton
    size="small"
    aria-label="edit"><Edit
    fontSize="inherit"/></IconButton></Link>

  const DeleteBtn = ({ id }) => (
    <Mutation key={'Delete' + id} mutation={REMOVE_POST} update={(
      cache,
      { data: { removeBlog } }) => {
      const { blog } = cache.readQuery({ query: BLOG })
      let index = blog.findIndex(request => request.id === removeBlog.id)
      if (index !== -1) {
        blog.splice(index, 1)
        cache.writeQuery({
          query: BLOG,
          data: { blog: [...blog] },
        })
        props.history.push('/admin/blog/')
      }
    }}>
      {(removeRequest, { data }) => (
        <IconButton onClick={() => removeRequest({ variables: { id } })}
                    size="small"
                    aria-label="edit"
        ><Delete fontSize="inherit"/>
        </IconButton>)
      }</Mutation>)

  return (<Query query={BLOG}>
    {({ loading, error, data }) => {
      if (loading) return 'Loading...'
      if (error) return `Error! ${error.message}`
      return (
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Requests</h4>
                <p className={classes.cardCategoryWhite}>
                  Here is requests for this table
                </p>
              </CardHeader>
              <CardBody>
                <Table
                  tableHeaderColor="primary"
                  tableHead={[
                    '#',
                    'Title',
                    'Date',
                    'Edit',
                    'Delete']}
                  tableData={
                    data.blog.map((request, index) => [
                      (index + 1).toString(),
                      request.name, moment(parseInt(request.date)).format('LL'),
                      <EditBtn id={request.id}/>,
                      <DeleteBtn id={request.id}/>,
                    ])
                  }
                />
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>)
    }}</Query>)
}

export default withStyles(styles)(Blog)
