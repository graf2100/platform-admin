/*!

=========================================================
* Material Dashboard React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react'
import ReactDOM from 'react-dom'
import { createBrowserHistory } from 'history'
import { Redirect, Route, Router, Switch } from 'react-router-dom'
// core components
import Admin from 'layouts/Admin.jsx'
import RTL from 'layouts/RTL.jsx'
import { ApolloProvider as ApolloProviderOld } from 'react-apollo'
import 'assets/css/material-dashboard-react.css?v=1.7.0'
import { client } from './config'
import { ApolloProvider } from '@apollo/react-hooks'

const hist = createBrowserHistory()

ReactDOM.render(
  <ApolloProvider client={client}>
    <ApolloProviderOld client={client}>
      <Router history={hist}>
        <Switch>
          <Route path="/admin" component={Admin}/>
          <Route path="/rtl" component={RTL}/>
          <Redirect from="/" to="/admin/dashboard"/>
        </Switch>
      </Router>
    </ApolloProviderOld>
  </ApolloProvider>,
  document.getElementById('root'),
)
